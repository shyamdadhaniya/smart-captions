import { Params, options } from "@/common/constants";
import axios from "axios";
import Image from "next/image";
import React, { Fragment, useEffect, useState } from "react";

interface IParams {
  limit: number;
  size: string;
  breed_id: string | undefined;
}
interface ImageData {
  height: number;
  id: string;
  url: string;
  width: number;
}
const ImagePlaceholderTransformation = () => {
  const [params, setParams] = useState<IParams>(Params);
  const [imageData, setImageData] = useState<ImageData[]>([]);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [error, setError] = useState<unknown>("");

  const fetchImageData = async () => {
    setIsLoading(true);
    try {
      const response = await axios({
        method: "get",
        params: params,
        transitional: {
          silentJSONParsing: true,
          forcedJSONParsing: true,
          clarifyTimeoutError: false,
        },
        adapter: ["xhr", "http"],
        timeout: 0,
        xsrfCookieName: "X-XSRF-TOKEN",
        maxContentLength: -1,
        maxBodyLength: -1,
        headers: { Accept: "application/json, text/plain" },
        baseURL: process.env.NEXT_PUBLIC_API_BASE_URL,
      });
      setImageData(response.data);
      return response.data;
    } catch (error: unknown) {
      setError(error);
      console.log("error", error);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    fetchImageData();
  }, []);

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    params.breed_id && fetchImageData();
  };

  const updateParamsWithBreedId = (value: string | undefined) => {
    const foundOption = options.find((option) => option.value === value);
    if (foundOption) {
      setParams((prevParams) => ({
        ...prevParams,
        breed_id: foundOption.key,
      }));
    }
  };

  return (
    <Fragment>
      {JSON.stringify(error)}
      <br />
      <br />
      {JSON.stringify(imageData)}
      <br />
      <br />
      <p> Loading .....{JSON.stringify(isLoading)}</p>
      <form onSubmit={handleSubmit}>
        <div className="w-full ml-2 mr-2 mt-10">
          <select
            onChange={(event) => {
              const { value } = event.target;
              updateParamsWithBreedId(value);
            }}
            id="countries"
            className="bg-gray-50 border border-gray-300 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-white dark:border-black dark:placeholder-black dark:text-black dark:focus:ring-black dark:focus:border-black"
          >
            {options.map((data, index) => {
              return (
                <option key={`options${index}`} value={data.value}>
                  {data.value}
                </option>
              );
            })}
          </select>
        </div>
        <div className="mt-5 ml-2 mr-2">
          <button
            type="submit"
            className={`flex w-full justify-center rounded-md bg-blue-600 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-blue-600 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600`}
          >
            Submit
          </button>
        </div>
      </form>
      <div className="grid grid-cols-4 gap-4 mt-10 ml-10">
        {imageData.map((data, index) => {
          return (
            <Image
              key={`image${index}`}
              src={data.url}
              height={data.height}
              width={data.width}
              alt={data.url}
              loading="eager"
            />
          );
        })}
      </div>
    </Fragment>
  );
};

export default ImagePlaceholderTransformation;
