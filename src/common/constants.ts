export const CONSTANTS = {
    select: {
        imageTransformation: {
            input: "Choose a option"
        }
    }
};
export const options = [
    {
        id: 1,
        key: "Choose a option",
        value: "Choose a option",
    },
    {
        id: 2,
        key: "abys",
        value: "Abyssinian",
    },
    {
        id: 3,
        key: "aege",
        value: "Aegean"
    },
];

export const Params = {
    limit: 8,
    size: "full",
    breed_id: ""
};