/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: false,
  images: {
    domains: ["cdn2.thecatapi.com"],
    remotePatterns: [
      {
        protocol: "https",
        hostname: "api.thecatapi.com",
      },
    ],
  },
};

module.exports = nextConfig;
